//
//  TabBarCustom.swift
//  Project-1
//
//  Created by Nguyễn Quang on 10/31/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

class TabBarCustom: UITabBarController, UITabBarControllerDelegate {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        self.selectedIndex = 1 
        setupMiddleButton()
    }
    
    
    // TabBarButton – Setup Middle Button
    func setupMiddleButton() {
        let image = UIImage(named: "study") as UIImage?
        let middleBtn = UIButton(frame: CGRect(x: (self.view.bounds.width / 2)-32, y: -33, width: 64, height: 64))
        middleBtn.layer.borderWidth = 0
        middleBtn.layer.cornerRadius = 32   
        middleBtn.setImage(image, for: .normal)
        //add to the tabbar and add click event
        self.tabBar.addSubview(middleBtn)
        middleBtn.addTarget(self, action: #selector(self.menuButtonAction), for: .touchUpInside)

        self.view.layoutIfNeeded()
    }

    // Menu Button Touch Action
    @objc func menuButtonAction(sender: UIButton) {
        self.selectedIndex = 1
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return MyTransition(viewControllers: tabBarController.viewControllers)
    }

    class MyTransition: NSObject, UIViewControllerAnimatedTransitioning {

        let viewControllers: [UIViewController]?
        let transitionDuration: Double = 0.4

        init(viewControllers: [UIViewController]?) {
            self.viewControllers = viewControllers
        }

        func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
            return TimeInterval(transitionDuration)
        }

        func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

            guard
                let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
                let fromView = fromVC.view,
                let fromIndex = getIndex(forViewController: fromVC),
                let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to),
                let toView = toVC.view,
                let toIndex = getIndex(forViewController: toVC)
                else {
                    transitionContext.completeTransition(false)
                    return
            }

            let frame = transitionContext.initialFrame(for: fromVC)
            var fromFrameEnd = frame
            var toFrameStart = frame
            fromFrameEnd.origin.x = toIndex > fromIndex ? frame.origin.x - frame.width : frame.origin.x + frame.width
            toFrameStart.origin.x = toIndex > fromIndex ? frame.origin.x + frame.width : frame.origin.x - frame.width
            toView.frame = toFrameStart

            DispatchQueue.main.async {
                transitionContext.containerView.addSubview(toView)
                UIView.animate(withDuration: self.transitionDuration, animations: {
                    fromView.frame = fromFrameEnd
                    toView.frame = frame
                }, completion: {success in
                    fromView.removeFromSuperview()
                    transitionContext.completeTransition(success)
                })
            }
        }

        func getIndex(forViewController vc: UIViewController) -> Int? {
            guard let vcs = self.viewControllers else { return nil }
            for (index, thisVC) in vcs.enumerated() {
                if thisVC == vc { return index }
            }
            return nil
        }
    }
    
}
