//
//  ReviewViewController.swift
//  Project-1
//
//  Created by Nguyễn Quang on 11/5/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
/// lib : https://github.com/ink-spot/UPCarouselFlowLayout

import UIKit

class ReviewViewController: UIViewController {
    var data:[cardView] = [
        cardView(image: "card1", des: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", title: "Từ Vựng"),
        cardView(image: "card2", des: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", title: "Phát Âm"),
        cardView(image: "card3", des: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", title: "Nghe"),
        cardView(image: "card4", des: "Lorem ipsum dolor sit amet, consectetur adipiscing elit", title: "Nói"),
    ]
    @IBOutlet weak var colectionViewCard: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        colectionViewCard?.dataSource = self
        colectionViewCard.register(UINib(nibName: "CollectionCardView", bundle: nil), forCellWithReuseIdentifier: "cardViewCell")
        let screenSize: CGRect = UIScreen.main.bounds
        let cellWidth = screenSize.width - 108
        let cellHeight = screenSize.height - 110
        let layout = colectionViewCard!.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
    }



}

extension ReviewViewController : UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cardViewCell", for: indexPath) as! CollectionCardView
        cell.featuredImageView.image = UIImage(named: data[indexPath.row].image)
        cell.titleCard.text = data[indexPath.row].title
        cell.desCard.text = data[indexPath.row].des
        return cell
    }
}


