//
//  HomeViewController.swift
//  Project-1
//
//  Created by Nguyễn Quang on 11/3/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var imageReviewEmty: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        imageReviewEmty.image = UIImage(named: "reviewEmty")
    }

}
