//
//  CollectionViewController.swift
//  Project-1
//
//  Created by Nguyễn Quang on 10/31/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit
import Alamofire

class CollectionViewController: UIViewController {
    var CollectionViewFlowLayout : UICollectionViewFlowLayout!
    @IBOutlet weak var CollectionViewCell: UICollectionView!
    let Identifier:String = "collectionViewCell"
    var dataTopic :Array<DataTopic> = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CollectionViewCell.dataSource = self
        CollectionViewCell.delegate = self
        CollectionViewCell.register(UINib(nibName: "CollectionCell", bundle: nil), forCellWithReuseIdentifier: Identifier)
        CollectionViewCell?.contentInset = UIEdgeInsets(top: 12, left: 16, bottom: 15, right: 16)
        if let layout = CollectionViewCell?.collectionViewLayout as? PinterestLayout {
          layout.delegate = self
        }
        onGetListTopic()
//
    }
    
    
    func onGetListTopic(){
        let headers: HTTPHeaders = [
              "access_token": "EMAWfpuT29POb0Yj1x0PLGlUtuhtm7yp1uIMrEjuiT9uoZCZAKFmmsYBQgqQR8mzDur1kJuucgHrPE0hZAXOSoarLSZAUCEZAixVg1YzQwRlAZDZD"
        ]
        let params = NSMutableDictionary.init()
        params.setValue("1", forKey: "level_id")
        ApiService.getListTopic(headers: headers, params: params as! Dictionary, dataCallback: { (data) in
            let topicResponse = Topic.init(dictionary: data as! NSDictionary)
            self.dataTopic = (topicResponse?.data)!
            self.CollectionViewCell.reloadData()
        }) { (error) in

        }
    }
    
}

extension CollectionViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataTopic.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = CollectionViewCell.dequeueReusableCell(withReuseIdentifier: Identifier, for: indexPath) as! CollectionCell
        cell.layer.cornerRadius = 15
        cell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cell.txtNameCollection.text = dataTopic[indexPath.row].name
        cell.txtNumberofstart.text  = (String(dataTopic[indexPath.row].number_of_stars!) + "/"+String(dataTopic[indexPath.row].total_questions!))
        if dataTopic[indexPath.row].image != nil {
            let url = URL(string: dataTopic[indexPath.row].image!)
            do{
                let data  = try Data(contentsOf: url!)
                cell.imageCollection.image = UIImage(data: data)
            }catch{
                print("errr")
            }
        }
        return cell
    }
}

extension CollectionViewController:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       let sampleVC = ReviewViewController(nibName: "ReviewViewController", bundle: nil)
        sampleVC.hidesBottomBarWhenPushed = true
       self.navigationController?.pushViewController(sampleVC, animated: true)
    }
}


extension CollectionViewController: PinterestLayoutDelegate {
   func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
    return 155
   }

}
