//
//  CustomBackNavigation.swift
//  Project-1
//
//  Created by Nguyễn Quang on 11/11/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

class CustomBackNavigation: UIView {

    @IBOutlet var backView: UIView!
    @IBOutlet weak var txtLable: UILabel!
    
    @IBInspectable var demo: String?{
        get{
            return txtLable.text
        }
        set(demo){
            txtLable.text = demo
        }
    }
    
    /**
     set image
     @IBInspectable var image: UIImage?{
         get{
             return myImage.image
         }
         set(demo){
             myImage.image = image
         }
     }
     */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        let view =  Bundle.main.loadNibNamed("CustomBackNavigation", owner: self, options: nil)?.first as! UIView
        self.addSubview(view)
//        Bundle.main.loadNibNamed("CustomBackNavigation", owner: self, options: nil)
//        backView.backViewLayout(self)
    }
        
    @IBAction func onBackPre(_ sender: Any) {
        (superview?.next as? UIViewController)?.navigationController?.popViewController(animated: true)
    }
    
}
//
//extension UIView
//{
//    func backViewLayout(_ container: UIView!) -> Void{
//        self.translatesAutoresizingMaskIntoConstraints = false;
//        self.frame = container.frame;
//        container.addSubview(self);
//        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
//        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
//        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
//        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
//    }
//}
