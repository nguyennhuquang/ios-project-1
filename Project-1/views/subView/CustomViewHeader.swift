//
//  CustomViewHeader.swift
//  Project-1
//  Created by Nguyễn Quang on 11/7/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
////  key search: Custom UIView Subclass with XIB

import UIKit

class CustomViewHeader: UIView {

    @IBOutlet weak var imgMesa: UIImageView!
    @IBOutlet var contentView: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
//        let view =  Bundle.main.loadNibNamed("CustomViewHeader", owner: self, options: nil)?.first as! UIView
//        self.addSubview(view)
        Bundle.main.loadNibNamed("CustomViewHeader", owner: self, options: nil)
        contentView.fixInView(self)
    }
    
    
}

extension UIView
{
    func fixInView(_ container: UIView!) -> Void{
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}
