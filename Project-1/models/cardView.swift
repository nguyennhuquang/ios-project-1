//
//  cardView.swift
//  Project-1
//
//  Created by Nguyễn Quang on 11/11/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import Foundation
class cardView {
    var image: String!
    var des:   String!
    var title: String!
    
    init(image:String, des:String, title:String) {
        self.image = image
        self.des = des
        self.title = title
    }
}
