//
//  BaseApi.swift
//  Project-1
//
//  Created by Nguyễn Quang on 11/4/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import Foundation
import Alamofire


class BaseApi: NSObject {
    static var baseUrl = "http://mesaenglish.com:8081/"
    var source : String = ""
    var header : HTTPHeaders = [:]
    var params  : Parameters = [:]
    var method : String = ""
    var request : Request? = nil
    init(source : String) {
        self.source = source
    }
    
    func apiDemo() -> String {
        return BaseApi.baseUrl + self.source
    }

    func setHeader(header: HTTPHeaders){
        self.header = header
    }

    func getHeader() -> HTTPHeaders {
        return header
    }

    func setParams(params: Parameters){
        self.params = params
    }

    func getParams() -> Parameters {
        return params
    }
}
