//
//  ApiService.swift
//  Project-1
//
//  Created by Nguyễn Quang on 11/4/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import Foundation
import Alamofire

class ApiService: BaseApi {
    static let getListTopic: String = "api/v1/topic/get-list-topic"
    
    override init(source: String) {
        super.init(source: source)
    }

    // get list topic
    static func getListTopic(headers: HTTPHeaders,params: Dictionary<String, AnyObject> ,dataCallback:@escaping ( _ data : Any) -> (), errorCallback:@escaping ( _ data : Any) -> () ) {
        let categoryService = ApiService.init(source: getListTopic)
        URLCache.shared.removeAllCachedResponses()
        Alamofire.request(
            categoryService.apiDemo(),
            method: .get,
            parameters: params,
            headers: headers
            )
            .responseJSON {
                (response) in
                guard response.result.isSuccess else {
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    errorCallback("Error")
                    return
                }
                dataCallback(response.value as Any)
        }
    }
    
    
    static func getNewFeed(dataCallback:@escaping ( _ data : Any) -> (), errorCallback:@escaping ( _ data : Any) -> ()){
        URLCache.shared.removeAllCachedResponses()
        Alamofire.request(
            "http://api.ancestrynote.com/api/v1/get-new-feed",
            method: .get
            ) 
            .responseJSON {
                (response) in
                guard response.result.isSuccess else {
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    errorCallback("Error")
                    return
                }
                dataCallback(response.value as Any)
        }
    }
}

