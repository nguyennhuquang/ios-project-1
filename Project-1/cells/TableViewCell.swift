//
//  TableViewCell.swift
//  Project-1
//
//  Created by Nguyễn Quang on 11/12/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var imageTableView: UIImageView!
    @IBOutlet weak var txtTitleTable: UILabel!
    @IBOutlet weak var txtDes: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)

        // Configure the view for the selected state
    }
    
    override open var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame =  newFrame
            frame.origin.y -= 5   // cách top.
            frame.size.height += 5
            super.frame = frame
        }
    }
    
}
