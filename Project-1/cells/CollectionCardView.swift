//
//  CollectionCardView.swift
//  Project-1
//
//  Created by Nguyễn Quang on 11/7/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

class CollectionCardView: UICollectionViewCell {
    @IBOutlet weak var featuredImageView: UIImageView!
    @IBOutlet weak var titleCard: UILabel!
    @IBOutlet weak var desCard: UILabel!
    @IBOutlet weak var startButton: UIButton!
    
    override func layoutSubviews() {
        featuredImageView.layer.cornerRadius = 20
        startButton.layer.cornerRadius = 17
//        super.layoutSubviews()
//        layer.cornerRadius = 20
//        layer.shadowRadius = 10
//        layer.shadowOpacity = 0.4
//        layer.shadowOffset = CGSize(width: 5, height: 10)
//        self.clipsToBounds = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
