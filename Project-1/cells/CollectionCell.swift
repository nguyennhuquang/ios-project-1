//
//  CollectionCell.swift
//  Project-1
//
//  Created by Nguyễn Quang on 10/31/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

class CollectionCell: UICollectionViewCell {
    @IBOutlet weak var txtNameCollection: UILabel!
    @IBOutlet weak var txtNumberofstart: UILabel!
    @IBOutlet weak var imageCollection: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
